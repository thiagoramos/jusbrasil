<?php
    
    if (! function_exists('money')) {
        
        function money($value = 0.00)
        {
            if(is_numeric($value)){
                return number_format($value,2,',','.');
            }else{
                return '0,00';
            }
        }
    }
    
	if (! function_exists('format_digits')) {
		
		function format_digits($value = null)
		{
			if($value){
				$value      = str_replace(array('.',','), array('.','.'), $value);
				$value      = explode('.', $value);
				$last_value = array_pop($value);
				$retorno    = '';
				
				foreach($value as $val){
					$retorno .= $val;
				}
				
				$retorno .= '.'.$last_value;
				
				return $retorno;
			}else{
				return 0;
			}
		}
	}
	
	if (! function_exists('br_mysql_date')) {
		
		function br_mysql_date($date = '00/00/0000')
		{
			return implode('-', array_reverse(explode('/', $date)));
		}
	}
	
	if (! function_exists('mysql_br_date')) {
		
		function mysql_br_date($date = '0000-00-00')
		{
			return implode('/', array_reverse(explode('-', $date)));
		}
	}
	
	if (! function_exists('mysql_br_date_time')) {
		
		function mysql_br_date_time($time = '0000-00-00 00:00:00', $noTime = false)
		{
			if($time){
				$time = explode(' ', $time);
				
				$horas = explode(':', $time[1]);
				
				if($noTime){
					return implode('/', array_reverse(explode('-', $time[0])));
				}
				
				return implode('/', array_reverse(explode('-', $time[0]))).' '.$horas[0].':'.$horas[1];
			}
			
			return false;
		}
	}
	
	
	if (! function_exists('only_numbers')) {
		function only_numbers($string = '')
		{
			return preg_replace('/\D/', '', $string);
		}
	}
	
	if (! function_exists('format_cpf')) {
		function format_cpf($str)
		{
			$str = only_numbers($str);
			$str = str_pad(preg_replace('[^0-9]', '', $str), 11, '0', STR_PAD_LEFT);
			
			return $str;
		}
	}
	
