jQuery(function() {
	
	$(".btnAddPhone").on("click", function(e) {
		e.preventDefault();
		var lastChild = $('.row-phone:last-child');
		var clone= $('.row-phone:last-child').clone();
		clone.find(".input-phone").val("");
		clone.html();
		
		$("#phonesGroup").append(clone);
	});
	
	$(document).on("click", ".btnRemovePhone", function(e) {
		e.preventDefault();
		if (($('#phonesGroup').find('.row-phone').length) > 1) {
			$(this).parents(".row-phone").fadeOut("fast", function () {
				$(this).detach();
			});
		}
	});
});