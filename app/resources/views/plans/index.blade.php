@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Planos de assinatura</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                        @if($plans && !empty($plans))
                            <div class="row">
                            @foreach($plans as $plan)
                                <div class="col-md-3" style="border-right: 1px solid #e4e4e4">
                                    <p>
                                        <h3>{{ $plan['name'] }}</h3> <br/>
                                    <div style="min-height: 100px;">
                                         {{ $plan['description'] }} <br/>
                                        <strong>R$ {{ money($plan['price']) }}</strong>
                                    <br/>
                                    </div>

                                    <a href="{{ route('plans.checkout',$plan['id']) }}" class="btn btn-success"  >Assinar</a>
                                    </p>
                                </div>
                            @endforeach
                            </div>
                        @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
