@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Confirmar assinatura do plano {{ $plan['name'] }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


                        <div class="row">

                            <div class="col-md-4" style="border-right: 1px solid #e4e4e4">
                                <p>
                                    <h4>Assinante: {{ $user->name }}</h4>
                                    <div >
                                         <strong>Email:</strong> {{ $user->email }} <br/>
                                         <strong>CPF:</strong> {{ $user->cpf }} <br/>
                                        <strong>Endereço do cobrança:</strong> {{ $user->address }} <br/>
                                        <strong>Telefone:</strong><br/> {!!  implode("<br/>", explode(",",$user->phones)) !!}
                                    <br/>
                                    </div>

                                </p>
                            </div>
                            <div class="col-md-3" style="border-right: 1px solid #e4e4e4">
                                <p>
                                <h3>Plano {{ $plan->name }}</h3>
                                <div >
                                    {{ $plan->description }} <br/>
                                    <strong>R$ {{ money($plan->price) }}</strong>
                                    <br/>
                                </div>

                                </p>
                            </div>

                            <div class="col-xl-5 col-lg-2 col-5">
                                {{ Form::model($plan, ['name' => 'form_checkout', 'id'=>'form_checkout', 'method' => 'put', 'route' => 'plans.checkout.submit']) }}
                                {{ Form::hidden('id', old('id')) }}
                                <form method="POST" action="{{ route('plans.checkout.submit') }}">
                                    @csrf
                                <div class="row">
                                    <div class="col-8">
                                        {!! Form::select('credit_card', $creditCards, old('credit_card'), ['class' => 'form-control', 'placeholder' => 'Cartão de Crédito' ,'required']) !!}
                                    </div>

                                    <div class=" col-4">
                                        {!! Form::select('parcels', ["1" => "A vista" ,  "2" => "x2" ,  "3" => "x3"], old('parcels',1), ['class' => 'form-control  text-left', 'placeholder' => 'Parcelas' , 'required']) !!}
                                    </div>

                                    <div class=" col-12">
                                        <br/>
                                      <strong>Selecione um cartão de crédito acima para o teste de pagamento</strong>
                                        <p>Caso deseje trocar o seu plano, é possível realizar esta ação a qualquer momento na tela de planos. </p>
                                    </div>

                                    <div class="col-12 pull-right text-lg-right"><br/>
                                        <button type="submit" class="btn btn-lg btn-success">Finalizar Compra</button>
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>

                        </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
