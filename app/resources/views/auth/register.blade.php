@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Cadastro') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register.submit') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nome completo') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="cpf" class="col-md-4 col-form-label text-md-right">{{ __('CPF') }}</label>

                            <div class="col-md-6">
                                <input id="cpf" type="cpf" class="form-control{{ $errors->has('cpf') ? ' is-invalid' : '' }}" name="cpf" value="{{ old('cpf') }}" >

                                @if ($errors->has('cpf'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cpf') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Endereço') }}</label>

                            <div class="col-md-6">
                                <input id="address" type="address" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address') }}" required>

                                @if ($errors->has('address'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">

                            <label for="phones" class="col-md-4 col-form-label text-md-right">{{ __('Telefones') }}</label>
                            <div class="form-group row pull-left">
                                <label  class="col-md-4 col-form-label text-md-right">
                                    <button type="button" class="btn btn-info btn-sm btnAddPhone" >+</button>
                                </label>
                            </div>
                            <div class="col-md-5 " id="phonesGroup">
                                @if(app('request')->old("phones") && is_array(app('request')->old("phones")))
                                    @foreach( (app('request')->old("phones" ) ) as $id => $phone)

                                        <div class="row col-offset-1 row-phone" style="margin-left: 5px;">
                                            <div class="col-md-10" style="margin-bottom: 15px;">
                                                {{ Form::text('phones['.($id).']', $phone, [ 'class' => 'form-control input-phone', 'placeholder' => 'Telefone' , 'maxlength' => 255] ) }}
                                            </div>

                                            <div class="col-md-2 pull-left"  style="margin-bottom: 15px;">
                                                <button type="button" class="btn btn-danger btn-xs btnRemovePhone" >x</button>
                                            </div>
                                        </div>


                                @endforeach
                                @else

                                    <div class="row col-offset-1 row-phone"  style="margin-left: 5px;">
                                        <div class="col-md-10" style="margin-bottom: 15px;">
                                            {{ Form::text('phones[]', old("phones"), [ 'class' => 'form-control input-phone', 'placeholder' => '(000) 00000-0000' , 'maxlength' => 255] ) }}
                                        </div>

                                        <div class="col-md-2"  style="margin-bottom: 15px;">
                                            <button type="button" class="btn btn-danger btn-xs btnRemovePhone" >x</button>
                                        </div>
                                    </div>


                                @endif


                                @if ($errors->has('phones'))
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('phones') }}</strong>
                                        </span>
                                @endif
                        </div>

                        </div>




                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Senha') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirme a senha') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Cadastrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
