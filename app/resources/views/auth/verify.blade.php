@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verifique seu email') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Um link foi enviado ao seu email para a confirmação da sua inscrição.') }}
                        </div>
                    @endif

                    {{ __('Para assinar um plano, confirme seu cadastro clicando n o link de verificação enviado para seu email') }}
                    {{ __('Se não recebeu o email') }}, <a href="{{ route('verification.resend') }}">{{ __('Clique aqui para tentar novamente') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
