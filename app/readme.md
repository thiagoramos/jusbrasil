## Jusbrasil - Cadastro de usu�rios 

Cadastro de usu�rios e compra de plano de assinatura

Este � um teste para a jusbrasil
referente a seguinte demanda: 

- [https://github.com/jusbrasil/careers/blob/master/php-fullstack/challenge.md](https://github.com/jusbrasil/careers/blob/master/php-fullstack/challenge.md).


## Depend�ncias

Este projeto utiliza como dependencias :
 - php 7.1.3
 - laravel framework
 - Docker CE 18.09.1
 - Docker composer

## Instru��es de instala��o

Se estiver em um S.O linux basta executar o script start.sh, caso contr�rio siga os passos abaixo:

- docker-compose up -d

- docker exec -it testejusbrasil cp .env.example .env

- sudo chmod -R o+rw app/bootstrap app/storage

- sudo chown -R $USER:$USER $(pwd) .

- docker exec -it testejusbrasil composer install -vvv && composer update -vvv

- docker exec -it testejusbrasil php artisan migrate

- docker exec -it testejusbrasil php artisan db:seed --class=MockupCreditCardSeeder

- docker exec -it testejusbrasil php artisan db:seed --class=InsertPlans

- docker ps -a 

- cd app && docker run --user $(id -u):$(id -g) -v $PWD:/app -v $PWD/.npm:/.npm -v $PWD/.config:/.config -w /app node:10.12.0-alpine npm install && npm run production

## Licen�a
<p align="center">

<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>