<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
    use Illuminate\Support\Facades\Auth;
 

Auth::routes();

Route::get('logout', function() {
    Auth::logout();
    return Redirect::to('/login');
});

Route::group(['prefix' => 'login'], function() {
    Route::get('/', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
    Route::post('/', ['as' => 'login.submit', 'uses' => 'Auth\LoginController@login']);
});

Route::post('sair', [
    'as' => 'logout',
    'uses' => 'Auth\LoginController@logout'
]);


Route::post('solicitar-nova-senha', [
    'as' => 'password.email',
    'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail'
]);

Route::group(['prefix' => 'esqueci-a-senha'], function() {
    Route::get('/', ['as' => 'password.request', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
    Route::post('/', ['as' => 'password.request.submit', 'uses' => 'Auth\ResetPasswordController@reset']);
    Route::get('/{token}', ['as' => 'password.reset', 'uses' => 'Auth\ResetPasswordController@showResetForm']);
});

Route::group(['prefix' => 'cadastrar'], function() {
    Route::get('/', ['as' => 'register', 'uses' => 'Auth\RegisterController@showRegistrationForm']);
    Route::post('/', ['as' => 'register.submit', 'uses' => 'Auth\RegisterController@register']);
});


Route::group(['middleware' => ['auth', 'web']], function() {

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('home');
    
    Route::group(['prefix' => 'planos'], function() {
        Route::get('/', ['as' => 'plans', 'uses' => 'PlanController@index']);
        Route::get('/confirmar-assinatura/{id}', ['as' => 'plans.checkout', 'uses' => 'PlanController@checkout'])->where('id', '[0-9]+');
        Route::match(['post', 'put'], '/finalizar-pedido', ['as' => 'plans.checkout.submit', 'before' => 'csfr', 'uses' => 'PlanController@order']);
    });

    
////
////    Route::group(['prefix' => 'perfil'], function() {
////        Route::get('/', ['as' => 'profile', 'uses' => 'UsersController@profile']);
////        Route::match(['post', 'put'], '/', ['as' => 'profileStore', 'before' => 'csfr', 'uses' => 'UsersController@profileStore']);
////    });
//
////
////    /**
////     * USUARIOS
////     */
////    Route::group(['prefix' => 'usuarios', 'middleware' => ['vperm:users_view']], function() {
////        Route::get('/', ['as' => 'users.list', 'uses' => 'UsersController@listUser']);
////        Route::get('/criar', ['as' => 'users.create', 'uses' => 'UsersController@newUser']);
////        Route::get('/editar/{id}', ['as' => 'users.update', 'uses' => 'UsersController@edit'])->where('id', '[0-9]+');
////    });
////
////    Route::group(['prefix' => 'usuarios', 'middleware' => ['vperm:users_admin']], function() {
////        Route::match(['post', 'put'], '/criar', ['as' => 'users.store', 'before' => 'csfr', 'uses' => 'UsersController@store']);
////        Route::get('/deletar/{id}', ['as' => 'users.destroy', 'uses' => 'UsersController@delete']);
////    });
////
//

});

