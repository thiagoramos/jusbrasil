<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('api')->get('/plzno', function (Request $request) {
//    return response()->json($request->user());
//});

Route::group(['prefix' => 'planos','middleware' => [ 'api','log.api']], function() {
    Route::get('/', ['as' => 'api.plans', 'uses' => 'Api\PlanController@getAllPlans']);
    Route::match(['post', 'put'], '/finalizar-pedido', ['as' => 'api.order',  'uses' => 'Api\OrderController@order']);
});

