<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InsertPlans extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (\DB::table('plan')->where('name', 'Avaliação')->count() == 0) {
            \DB::table('plan')->insert([
                  'name' => "Avaliação",
                  'price' => '00.00',
                  'description' => 'Assinatura de avaliação',
                  'created_at'    => Carbon::now()
              ]);
        }
       
        if (DB::table('plan')->where('name', 'Mensal')->count() == 0) {
            DB::table('plan')->insert([
                'name' => "Mensal",
                'price' => '19.90',
                'description' => 'Assinatura para um mês de acesso ao portal',
                'created_at'    => Carbon::now()
            ]);
        }
    
        if (DB::table('plan')->where('name', 'Trimestral')->count() == 0) {
            DB::table('plan')->insert([
                  'name' => "Trimestral",
                  'price' => '19.90',
                  'description' => 'Assinatura para 3 meses de acesso ao portal',
                  'created_at'    => Carbon::now()
              ]);
        }
    
        if (DB::table('plan')->where('name', 'Anual')->count() == 0) {
            DB::table('plan')->insert([
                  'name' => "Anual",
                  'price' => '19.90',
                  'description' => 'Assinatura para um ano de acesso ao portal',
                  'created_at'    => Carbon::now()
              ]);
        }
    }
}
