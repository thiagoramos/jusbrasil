<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MockupCreditCardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('mockup_credit_card')->where('number', '4916806016146693')->count() == 0) {
            DB::table('mockup_credit_card')->insert([
               'number' => "4916806016146693",
               'code' => '126',
               'flag' => 'Visa',
               'type' => 'valid',
               'created_at' => Carbon::now()
           ]);
        }
    
        if (DB::table('mockup_credit_card')->where('number', '4539097540431861')->count() == 0) {
            DB::table('mockup_credit_card')->insert([
                'number' => "4916326493266278",
                'code' => '388',
                'flag' => 'Visa',
                'type' => 'valid',
                'created_at'    => Carbon::now()
            ]);
        }
        
        if (DB::table('mockup_credit_card')->where('number', '5230140252745158')->count() == 0) {
            DB::table('mockup_credit_card')->insert([
                'number' => "5230140252745158",
                'code' => '488',
                'flag' => 'MasterCard',
                'type' => 'random',
                'created_at'    => Carbon::now()
            ]);
        }
    
        if (DB::table('mockup_credit_card')->where('number', '4389356810433143')->count() == 0) {
            DB::table('mockup_credit_card')->insert([
                'number' => "4389356810433143",
                'code' => '327',
                'flag' => 'Elo',
                'type' => 'invalid',
                'created_at'    => Carbon::now()
            ]);
        }
    }
}
