<?php

namespace App\Services;

use Illuminate\Support\Facades\Auth;

use App\Entities\User;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\UserRequest;

class UserService
{
    public function __construct(Router $router, User $e)
    {
        $this->router   = $router;
        $this->entity   = $e;
    }
	
	public function get($id){
		$User =  User::withTrashed()->where('id' , $id )->first();
		return $User;
	}
   
    function destroy($id)
    {
        return $this->entity->find($id)->delete();
    }
    
    
    function restore($id)
    {
        return $this->entity->withTrashed()->find($id)->restore();
    }
  
    public function save(UserRequest $data){
        if($data->get("id")) {
            return self::update($data);
        }else{
            return self::create($data);
        }
    }
  

	private function dataTransform(array $data) {
		$transformed_data = [
			'name' => $data['name'],
			'email' => $data['email'],
			'cpf' => $data['cpf'],
			'address' => $data['address'],
			'phones' => implode(",", $data['phones'])
		];
		
		$transformed_data['cpf'] = $transformed_data['cpf'];
		
		if(isset($data['password']) && $data['password'] != ""){
			$transformed_data['password'] = bcrypt($data['password']);
		}
		
		return $transformed_data;
	}

    private function create(UserRequest $data){
        try{
            
            $valid_data = self::dataTransform($data->all());
            $user = $this->entity->create($valid_data);
            return ['status' => 'ok' , 'user' => $user];
        }catch(\Exception $ex){
            return [
                'status' => 'error',
                'message' => $ex->getMessage(),
            ];
        }
    }
  
    private function update(UserRequest $data){
        try{
            $valid_data = self::dataTransform($data->all());
            User::withTrashed()->where('id', $data->get("id"))->update($valid_data);
            return ['status' => 'ok'];
        }catch(\Exception $ex){
            return [
                'status' => 'error',
                'message' => $ex->getMessage(),
            ];
        }
    }
}