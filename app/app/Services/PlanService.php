<?php
/**
 * Service de planos
 */
namespace App\Services;

use App\Entities\Plan;
use Illuminate\Support\Facades\Auth;
use Prettus\Validator\Exceptions\ValidatorException;
use Prettus\Validator\Contracts\ValidatorInterface;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\MessageBag;
use Illuminate\Routing\Router;
use App\Http\Requests\PlanRequest;
use Symfony\Component\Routing\Route;
use GuzzleHttp\Client;
use App\Entities\UserOrder;

use Illuminate\Http\Request;

/**
 * Class PlanService
 * @package App\Services
 */
class PlanService {

    protected $router;
    protected $entity;
    
    /**
     * PlanService constructor.
     * @param \Illuminate\Routing\Router $router
     * @param \App\Entities\Plan         $e
     */
    public function __construct(Router $router, Plan $e) {
        $this->router   = $router;
        $this->entity   = $e;
    }
    
    public function get($id){
        $plan = $this->entity->where('id' , $id )->first();
        return $plan;
    }
    
    public function getPlans(){
        $plans =  Plan::whereNull('deleted_at')->pluck('name', 'id');
        return $plans;
    }
        
    public function getAll(array $data){
      $plans =  Plan::whereNull('deleted_at')->get()->toArray();
        return $plans;
    }
    
    public function save(PlanRequest $data){
        try{
            $valid_data = self::dataTransform($data->all());
            
            $transaction = $this->curlPost( array("order" => $valid_data));
            if ($transaction) {
                $transaction = json_decode($transaction, true);
              
                $order = [
                    'user_id' => $valid_data['user_id'],
                    'plan_id' => $valid_data['plan_id'],
                    'status' => (isset($transaction['status']))?$transaction['status']:"error",
                    'credit_card' => $valid_data['credit_card'],
                    'transaction_code' => (isset($transaction['transaction']))?$transaction['transaction']:0
                ];
                
                $this->saveOrder($order);
                return ['status' => 'ok', 'order' => $this->curlPost(array("order" => $valid_data))];
            }
            
        }catch(\Exception $ex){
            return [
                'status' => 'error',
                'message' => $ex->getMessage(),
            ];
        }
    }
    
    private function dataTransform(array $data) {
        $transformed_data = [
            'credit_card' => $data['credit_card'],
            'parcels' => $data['parcels'],
            'user_id' => Auth::user()->id,
            'plan_id' => $data['id']
        ];
        
        return $transformed_data;
    }
    
    private function curlPost($data1)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, route( "api.order" ) );
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data1));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
        $server_output = curl_exec($ch);
    
        $err = curl_error( $ch );
        curl_close( $ch );
        return $server_output;
    }
    
    private function saveOrder($order) {
        UserOrder::create($order);
    }
}
