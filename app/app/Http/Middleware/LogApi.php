<?php

namespace App\Http\Middleware;

use Closure;
use App\Entities\ApiLog;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class LogApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        ApiLog::create([
            'request' => \GuzzleHttp\json_encode($request->json()->all())
        ]);

        return $next($request);
    }
}
