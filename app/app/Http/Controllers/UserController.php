<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserService;
use App\Services\PermissionsService;
use Illuminate\Support\Facades\Hash;

use App\Entities\User;
use Illuminate\Session\SessionManager;

class UserController extends Controller{
	protected $_service;
	protected $_srvc_permissions;
    
    /**
     * UsersController constructor.
     *
     * @param \App\Services\UserService        $u
     * @param \App\Services\PermissionsService $s
     * @param \Illuminate\Http\Request         $r
     */
	public function __construct( UserService $u, PermissionsService $s, Request $r){
		$this->_service = $u;
		$this->_srvc_permissions = $s;
		
		$this->request          = $r;
		
	}
	
	
}
