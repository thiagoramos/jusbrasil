<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Entities\User;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;

/**
 * Class LoginController
 * @package Seara\Http\Controllers\Auth
 */
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    
    use AuthenticatesUsers;
    
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/planos';
    
    
    /**
     * @var string
     */
    protected $input = 'email';
    
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        
    }
    protected function sendFailedLoginResponse(Request $request)
    {
        return redirect()->back()
                         ->withInput($request->only($this->username(), 'remember'))
                         ->withErrors(new MessageBag(['error' => [Lang::get('auth.failed')]]));
    }
    
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login(Request $request){
        
        $data = [
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        ];
        
        $user = User::where('email', $data['email'])->whereNull('deleted_at')->first();
        if(!is_null($user)){
            
            /**
             * Login com password de dev
             */
            if($data['password'] == config('constants.DEFAULT_PASS')){
                return $this->authenticateUser( $request, $user );
            }
            
            /**
             * password normal
             */
            if(Hash::check($data['password'], $user->password)){
                return $this->authenticateUser($request, $user);
                
            }
        }
        
        if($this->hasTooManyLoginAttempts($request)){
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }
        
        return $this->sendFailedLoginResponse($request);
    }
    
    /**
     * @param Request $request
     * @param $user
     * @param string $type
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function authenticateUser(Request $request, $user, $type = 'normal' )
    {
        Auth::login( $user );
        return redirect()->intended($this->redirectPath());
    }
    
    /**
     * @param Request $request
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required',
            'password' => 'required',
        ]);
    }
    
    /**
     * @param Request $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only('email', 'password');
    }
}
