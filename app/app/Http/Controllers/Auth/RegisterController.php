<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests\UserRequest;
use App\Services\UserService;

class RegisterController extends Controller
{
   

    use RegistersUsers;

    protected $redirectTo = '/planos';
    
    protected $service ;
    
   
    public function __construct(UserService $service)
    {
        $this->service = $service;
        $this->middleware('guest');
    }
  
   
    function register(UserRequest $request)
    {
        $retorno = $this->service->save($request);
       
        if($retorno['status'] == 'ok' && isset($retorno['user'])){
            $this->guard()->login($retorno['user']);
    
            return $this->registered($request, $retorno['user'])
                ?: redirect($this->redirectPath());
        }
    
        return redirect($this->redirectPath());
    }
}
