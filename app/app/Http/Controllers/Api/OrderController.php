<?php
/**
 * Controller da api de pagamento
 */
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Plan;
use App\Services\PlanService;
use App\Entities\MockupCreditCard;

/**
 * Class OrderController
 * @package App\Http\Controllers\Api
 */
class OrderController extends Controller{
    
    protected $_service;
    
    /**
     * OrderController constructor.
     * @param \App\Services\PlanService $s
     */
    public function __construct( PlanService $s){
        $this->_service = $s;
    }

    public function getAllPlans(){
        $plans = $this->_service->getPlans();
        
        return response()->json($plans, 200);
    }
    
    public function order(Request $request)
    {
    
        if (count($request->json()->all())) {
            $postbody = $request->json()->all();
            if ( isset($postbody['order']['credit_card'])) {
                $card = MockupCreditCard::select('*')->whereNull('deleted_at')->where("number", $postbody['order']['credit_card'])->first();
                $card = $card->toArray();
                
                if ($card['type'] == "valid") {
                    return  response()->json([
                        'transaction' => date("YmdHis"),
                        'status' => "success"
                    ]);
                }
    
                if ($card['type'] == "invalid") {
                    return  response()->json([
                        'status' => "error"
                    ]);
                }
    
                if ($card['type'] == "random") {
                     if (rand(1,0) == 1) {
                         return  response()->json([
                             'transaction' => date("YmdHis"),
                             'status' => "success"
                         ]);
                     }
                }
            }
        }
        
        
        return response()->json($request->wantsJson());
    }
    
    public function charge(Request $request)
    {
        if (count($request->json()->all())) {
            $postbody = $request->json()->all();
            if ( isset($postbody['order']['credit_card'])) {
                $card = MockupCreditCard::select('*')->whereNull('deleted_at')->where("number", $postbody['order']['credit_card'])->first();
                $card = $card->toArray();
                
                if ($card['type'] == "valid") {
                    return  response()->json([
                        'transaction' => date("YmdHis"),
                        'status' => "success"
                    ]);
                }
                
                if ($card['type'] == "invalid") {
                    return  response()->json([
                        'status' => "error"
                    ]);
                }
                
                if ($card['type'] == "random") {
                    if (rand(1,0) == 1) {
                        return  response()->json([
                            'transaction' => date("YmdHis"),
                            'status' => "success"
                        ]);
                    }
                }
            }
        }
        
        
        return response()->json($request->wantsJson());
    }
    
}
