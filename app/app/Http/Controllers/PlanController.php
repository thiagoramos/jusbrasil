<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Plan;
use App\Entities\MockupCreditCard;
use App\Services\PlanService;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\PlanRequest;

use Illuminate\Support\MessageBag;

class PlanController extends Controller{
    
    protected $service;
    
    public function __construct( PlanService $s){
        $this->service = $s;
    }
    
    public function index(Request $request){
        $plans = $this->service->getAll($request->all());
        
        return view('plans/index', compact('plans'));
    }
    
    public function checkout($id){
        $plan = $this->service->get($id);
        $user = Auth::user();
        $creditCards =  MockupCreditCard::whereNull('deleted_at')->pluck('number', 'number');
        return view('plans/checkout', compact('plan','user','creditCards'));
    }
    
    
    function order(PlanRequest $request)
    {
        $retorno = $this->service->save($request);
     
        if($retorno['status'] == 'ok'){
            return redirect()->route('home')->withErrors(new MessageBag([
                            'success' => ['Plano assinado com sucesso!']]
         ));
        }
    
        return redirect()->back()
                         ->withErrors(new MessageBag(['error' => [$retorno['message']]]))
                         ->withInput($request->all());
        
    }
}
