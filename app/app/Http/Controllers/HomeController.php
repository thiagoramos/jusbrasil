<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\User;
use Illuminate\Session\SessionManager;

class HomeController extends Controller
{
    
    public function __construct(Request $r)
    {
        $this->middleware('auth');
        
        $this->request          = $r;
    }
    
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->route('plans');
    }
    
    
    
    
}
