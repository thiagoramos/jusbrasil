<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        $required = [
            'id' => 'integer',
            'name' => 'required|max:255',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'cpf' => 'required|max:255',
            'address' => 'required|max:255',
            'phones' => 'required|array',
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ];
        
        return $required;
    }
   
}
