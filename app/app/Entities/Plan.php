<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Plan extends Model implements Transformable
{
    use TransformableTrait;
    
    protected $table = "plan";
    protected $fillable = [
        'name',
        'price',
        'description',
    ];
    
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
