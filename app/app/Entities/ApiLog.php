<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ApiLog extends Model implements Transformable
{
    use TransformableTrait;
    
    protected $table = "api_log";
    protected $fillable = [
        'request'
    ];
}
