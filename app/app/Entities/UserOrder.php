<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class UserOrder extends Model implements Transformable
{
    use TransformableTrait;
    
    protected $table = "user_order";
    protected $fillable = [
        'user_id',
        'plan_id',
        'status',
        'credit_card',
        'transaction_code'
    ];
    
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
