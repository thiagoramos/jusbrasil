<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class MockupCreditCard extends Model implements Transformable
{
    use TransformableTrait;
    
    protected $table = "mockup_credit_card";
    protected $fillable = [
        'number',
        'code',
        'type',
        'flag',
    ];
    
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
