## Jusbrasil - Cadastro de usuários 

Cadastro de usuários e compra de plano de assinatura

Este é um teste para a jusbrasil
referente a seguinte demanda: 

- [https://github.com/jusbrasil/careers/blob/master/php-fullstack/challenge.md](https://github.com/jusbrasil/careers/blob/master/php-fullstack/challenge.md).

## Considerações

Nunca havia trabalhado com Docker antes, este é meu primeiro projeto com esta tecnologia.
Algumas solicitações do teste não foram possíveis de serem feitas
por que perdi muito tempo para resolver as questões do Docker.

Mensagens de sucesso para o usuário por exemplo não estão sendo retornadas,
mas está funcional.

## Dependências

Este projeto utiliza como dependencias :
 - php 7.1.3
 - laravel framework
 - Docker CE 18.09.1
 - Docker composer

## Instruções de instalação

Se estiver em um S.O linux basta executar o script start.sh, caso contrário siga os passos abaixo:

- docker-compose up -d

- docker exec -it testejusbrasil cp .env.example .env

- sudo chmod -R o+rw app/bootstrap app/storage

- sudo chown -R $USER:$USER $(pwd) .

- docker exec -it testejusbrasil composer install -vvv && composer update -vvv

- docker exec -it testejusbrasil php artisan migrate

- docker exec -it testejusbrasil php artisan db:seed --class=MockupCreditCardSeeder

- docker exec -it testejusbrasil php artisan db:seed --class=InsertPlans

- docker ps -a 

- cd app && docker run --user $(id -u):$(id -g) -v $PWD:/app -v $PWD/.npm:/.npm -v $PWD/.config:/.config -w /app node:10.12.0-alpine npm install && npm run production

## Licença
<p align="center">

<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>