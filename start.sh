#!/bin/bash

echo Uploading Application container 
docker-compose up -d

echo Copying the configuration example file
docker exec -it testejusbrasil cp .env.example .env

sudo chmod -R o+rw app/bootstrap app/storage

sudo chown -R $USER:$USER $(pwd) .

echo Install dependencies
docker exec -it testejusbrasil composer install

echo Make migrations
docker exec -it testejusbrasil php artisan migrate

echo Make seeds
docker exec -it testejusbrasil php artisan db:seed

echo Information of new containers
docker ps -a


cd app && docker run --user $(id -u):$(id -g) -v $PWD:/app -v $PWD/.npm:/.npm -v $PWD/.config:/.config -w /app node:10.12.0-alpine npm install && npm run production
 
